Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 161
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> DESCRIBE playlists_songs;
+-------------+---------+------+-----+---------+----------------+
| Field       | Type    | Null | Key | Default | Extra          |
+-------------+---------+------+-----+---------+----------------+
| id          | int(11) | NO   | PRI | NULL    | auto_increment |
| playlist_id | int(11) | NO   | MUL | NULL    |                |
| song_id     | int(11) | NO   |     | NULL    |                |
+-------------+---------+------+-----+---------+----------------+
3 rows in set (0.213 sec)

MariaDB [music_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [music_db]> USE test;
Database changed
MariaDB [test]> SHOW TABLES;
Empty set (0.001 sec)

MariaDB [test]> SHOW TABLES
    -> ;
Empty set (0.001 sec)

MariaDB [test]> SHOW TABLES;
Empty set (0.001 sec)

MariaDB [test]> USE mysql;
Database changed
MariaDB [mysql]> DESCRIBE user;
+------------------------+---------------------+------+-----+----------+-------+
| Field                  | Type                | Null | Key | Default  | Extra |
+------------------------+---------------------+------+-----+----------+-------+
| Host                   | char(60)            | NO   |     |          |       |
| User                   | char(80)            | NO   |     |          |       |
| Password               | longtext            | YES  |     | NULL     |       |
| Select_priv            | varchar(1)          | YES  |     | NULL     |       |
| Insert_priv            | varchar(1)          | YES  |     | NULL     |       |
| Update_priv            | varchar(1)          | YES  |     | NULL     |       |
| Delete_priv            | varchar(1)          | YES  |     | NULL     |       |
| Create_priv            | varchar(1)          | YES  |     | NULL     |       |
| Drop_priv              | varchar(1)          | YES  |     | NULL     |       |
| Reload_priv            | varchar(1)          | YES  |     | NULL     |       |
| Shutdown_priv          | varchar(1)          | YES  |     | NULL     |       |
| Process_priv           | varchar(1)          | YES  |     | NULL     |       |
| File_priv              | varchar(1)          | YES  |     | NULL     |       |
| Grant_priv             | varchar(1)          | YES  |     | NULL     |       |
| References_priv        | varchar(1)          | YES  |     | NULL     |       |
| Index_priv             | varchar(1)          | YES  |     | NULL     |       |
| Alter_priv             | varchar(1)          | YES  |     | NULL     |       |
| Show_db_priv           | varchar(1)          | YES  |     | NULL     |       |
| Super_priv             | varchar(1)          | YES  |     | NULL     |       |
| Create_tmp_table_priv  | varchar(1)          | YES  |     | NULL     |       |
| Lock_tables_priv       | varchar(1)          | YES  |     | NULL     |       |
| Execute_priv           | varchar(1)          | YES  |     | NULL     |       |
| Repl_slave_priv        | varchar(1)          | YES  |     | NULL     |       |
| Repl_client_priv       | varchar(1)          | YES  |     | NULL     |       |
| Create_view_priv       | varchar(1)          | YES  |     | NULL     |       |
| Show_view_priv         | varchar(1)          | YES  |     | NULL     |       |
| Create_routine_priv    | varchar(1)          | YES  |     | NULL     |       |
| Alter_routine_priv     | varchar(1)          | YES  |     | NULL     |       |
| Create_user_priv       | varchar(1)          | YES  |     | NULL     |       |
| Event_priv             | varchar(1)          | YES  |     | NULL     |       |
| Trigger_priv           | varchar(1)          | YES  |     | NULL     |       |
| Create_tablespace_priv | varchar(1)          | YES  |     | NULL     |       |
| Delete_history_priv    | varchar(1)          | YES  |     | NULL     |       |
| ssl_type               | varchar(9)          | YES  |     | NULL     |       |
| ssl_cipher             | longtext            | NO   |     |          |       |
| x509_issuer            | longtext            | NO   |     |          |       |
| x509_subject           | longtext            | NO   |     |          |       |
| max_questions          | bigint(20) unsigned | NO   |     | 0        |       |
| max_updates            | bigint(20) unsigned | NO   |     | 0        |       |
| max_connections        | bigint(20) unsigned | NO   |     | 0        |       |
| max_user_connections   | bigint(21)          | NO   |     | 0        |       |
| plugin                 | longtext            | NO   |     |          |       |
| authentication_string  | longtext            | NO   |     |          |       |
| password_expired       | varchar(1)          | NO   |     |          |       |
| is_role                | varchar(1)          | YES  |     | NULL     |       |
| default_role           | longtext            | NO   |     |          |       |
| max_statement_time     | decimal(12,6)       | NO   |     | 0.000000 |       |
+------------------------+---------------------+------+-----+----------+-------+
47 rows in set (0.269 sec)

MariaDB [mysql]> SHOW TABLES;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| column_stats              |
| columns_priv              |
| db                        |
| event                     |
| func                      |
| general_log               |
| global_priv               |
| gtid_slave_pos            |
| help_category             |
| help_keyword              |
| help_relation             |
| help_topic                |
| index_stats               |
| innodb_index_stats        |
| innodb_table_stats        |
| plugin                    |
| proc                      |
| procs_priv                |
| proxies_priv              |
| roles_mapping             |
| servers                   |
| slow_log                  |
| table_stats               |
| tables_priv               |
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
| transaction_registry      |
| user                      |
+---------------------------+
31 rows in set (0.097 sec)

MariaDB [mysql]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [mysql]> USE music_db;
Database changed
MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> INSERT INTO artists (name)
    -> VALUES ("Rivermaya");
Query OK, 1 row affected (0.093 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
+----+-----------+
1 row in set (0.001 sec)

MariaDB [music_db]> INSERT INTO artists (name)
    -> VALUES ("Psy");
Query OK, 1 row affected (0.071 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
|  2 | Psy       |
+----+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE albums;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int(11)     | NO   | PRI | NULL    | auto_increment |
| name      | varchar(50) | NO   |     | NULL    |                |
| year      | date        | NO   |     | NULL    |                |
| artist_id | int(11)     | NO   | MUL | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
4 rows in set (0.165 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
|  2 | Psy       |
+----+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE albums;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int(11)     | NO   | PRI | NULL    | auto_increment |
| name      | varchar(50) | NO   |     | NULL    |                |
| year      | date        | NO   |     | NULL    |                |
| artist_id | int(11)     | NO   | MUL | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
4 rows in set (0.219 sec)

MariaDB [music_db]> INSERT INTO albums (name, year, artist_id)
    -> VALUES ("Psy 6", "2012-1-1", 2);
Query OK, 1 row affected (0.137 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
+----+-------+------------+-----------+
1 row in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
|  2 | Psy       |
+----+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE albums;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int(11)     | NO   | PRI | NULL    | auto_increment |
| name      | varchar(50) | NO   |     | NULL    |                |
| year      | date        | NO   |     | NULL    |                |
| artist_id | int(11)     | NO   | MUL | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
4 rows in set (0.159 sec)

MariaDB [music_db]> INSERT INTO albums (name, year, artist_id)
    -> VALUES ("Trip", "1996-1-1", 1);
Query OK, 1 row affected (0.130 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
|  2 | Trip  | 1996-01-01 |         1 |
+----+-------+------------+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM albums;
+----+-------+------------+-----------+
| id | name  | year       | artist_id |
+----+-------+------------+-----------+
|  1 | Psy 6 | 2012-01-01 |         2 |
|  2 | Trip  | 1996-01-01 |         1 |
+----+-------+------------+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT * FROM artists;
+----+-----------+
| id | name      |
+----+-----------+
|  1 | Rivermaya |
|  2 | Psy       |
+----+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT name FROM artists;
+-----------+
| name      |
+-----------+
| Rivermaya |
| Psy       |
+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT name, year FROM albums;
+-------+------------+
| name  | year       |
+-------+------------+
| Psy 6 | 2012-01-01 |
| Trip  | 1996-01-01 |
+-------+------------+
2 rows in set (0.001 sec)

MariaDB [music_db]> SELECT name, id FROM albums;
+-------+----+
| name  | id |
+-------+----+
| Psy 6 |  1 |
| Trip  |  2 |
+-------+----+
2 rows in set (0.001 sec)

MariaDB [music_db]> INSERT INTO songs (title, length, genre, album_id)
    -> VALUES ("Gangnam Style", 253, "K-Pop", 1),
    -> ("Kundiman", 234, "OPM", 2),
    -> ("Kisapmata", 319, "OPM", 2);
Query OK, 3 rows affected (0.172 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [music_db]> SELECT * FROM songs;
+----+---------------+----------+-------+----------+
| id | title         | length   | genre | album_id |
+----+---------------+----------+-------+----------+
|  1 | Gangnam Style | 00:02:53 | K-Pop |        1 |
|  2 | Kundiman      | 00:02:34 | OPM   |        2 |
|  3 | Kisapmata     | 00:03:19 | OPM   |        2 |
+----+---------------+----------+-------+----------+
3 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title
    -> FROM songs
    -> WHERE genre = "OPM";
+-----------+
| title     |
+-----------+
| Kundiman  |
| Kisapmata |
+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE songs;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| title    | varchar(50) | NO   |     | NULL    |                |
| length   | time        | NO   |     | NULL    |                |
| genre    | varchar(50) | NO   |     | NULL    |                |
| album_id | int(11)     | NO   | MUL | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
5 rows in set (0.217 sec)

MariaDB [music_db]> SELECT * FROM songs;
+----+---------------+----------+-------+----------+
| id | title         | length   | genre | album_id |
+----+---------------+----------+-------+----------+
|  1 | Gangnam Style | 00:02:53 | K-Pop |        1 |
|  2 | Kundiman      | 00:02:34 | OPM   |        2 |
|  3 | Kisapmata     | 00:03:19 | OPM   |        2 |
+----+---------------+----------+-------+----------+
3 rows in set (0.001 sec)

MariaDB [music_db]> SELECT title
    -> FROM songs
    -> WHERE genre = "OPM";
+-----------+
| title     |
+-----------+
| Kundiman  |
| Kisapmata |
+-----------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE songs;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| title    | varchar(50) | NO   |     | NULL    |                |
| length   | time        | NO   |     | NULL    |                |
| genre    | varchar(50) | NO   |     | NULL    |                |
| album_id | int(11)     | NO   | MUL | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
5 rows in set (0.151 sec)

MariaDB [music_db]> UPDATE songs
    -> SET length = 240
    -> WHERE title = "Kundiman";
Query OK, 1 row affected (0.097 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [music_db]> SELECT * FROM songs WHERE title = "Kundiman";
+----+----------+----------+-------+----------+
| id | title    | length   | genre | album_id |
+----+----------+----------+-------+----------+
|  2 | Kundiman | 00:02:40 | OPM   |        2 |
+----+----------+----------+-------+----------+
1 row in set (0.001 sec)

MariaDB [music_db]>
